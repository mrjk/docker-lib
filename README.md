
## Quickstart
Really quick:
```
. env.sh
docker-make-make -h
docker-make-make -d --no-push
```

## Buils docker-make

This works only with python2.7 at this time, but I plan to port it to a more recent version
of Python.

Create virtualenv:
```
virtualenv  --no-site-packages .venv
. env.sh
```
Then:
```
docker-make -d --no-push
```

## Extract image metadata:
There are a couple of helpers too:
```
show_meta [pattern]
```



