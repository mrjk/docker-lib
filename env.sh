#!/bin/sh

if [ -f '.venv/bin/activate' ]; then
  . .venv/bin/activate
fi


alias docker-make-docker="docker run --rm -w /usr/src/app\\
  -v ~/.docker:/root/.docker\\
  -v /var/run/docker.sock:/var/run/docker.sock\\
  -v \"\$(pwd)\":/usr/src/app jizhilong/docker-make docker-make"

show_meta() {
  local prefix="${1:-.*}"
  local images=$(docker images | tail -n +2 | awk '{print $1 ":" $2}' | sort | uniq | grep "$prefix")

  for i in $images; do
    echo "Image: $i"
    docker inspect $i | jq '.[0] | .Config.Labels, .Config.Entrypoint, .Config.Env, .Config.Volumes, .Config.WorkingDir'
  done

}


run_shell(){
  # This spawn a simple docker without persistance
  local OS=${1:-debian}
  local debug_shell=$(docker inspect $OS | jq -r '.[0] | .Config.Labels."net.jzn42.docker.shell"')
  echo "Running one shot instance of '$OS' with '$debug_shell'"
  docker run --rm -i -t  ${OS} ${debug_shell}

  # This spwan a permanent VM
  #docker run -i -t  debian /bin/bash

}


