FROM debian:9
MAINTAINER mrjk

# Create environment variables
ENV CONTAINER docker
ENV USER root
ENV HOME /root
ENV TERM xterm

# Update image
RUN DEBIAN_FRONTEND=noninteractive apt-get -qq update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y upgrade

# Install vagrant
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y vagrant

RUN mkdir -p /root/work

# Finish
WORKDIR /root/work
CMD ["/bin/bash"]



# Clean stuffs
# && rm -rf /var/lib/apt/lists/*

# Cleanup
#RUN DEBIAN_FRONTEND=noninteractive apt-get purge -y build-essential
#RUN DEBIAN_FRONTEND=noninteractive apt-get purge -y gfortran
#RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

