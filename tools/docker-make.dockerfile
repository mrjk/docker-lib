FROM python:2.7.11
MAINTAINER Ji.Zhilong <zhilongji@gmail.com>

RUN apt-get update && apt-get install -y git
ADD docker-make/requirements.pip /tmp/
RUN pip install -r /tmp/requirements.pip

ADD docker-make /usr/local/src/docker-make

RUN pip install /usr/local/src/docker-make
